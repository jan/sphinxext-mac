jandd.sphinxext.ip
==================

This is `Jan Dittberner`_'s MAC address extension for `Sphinx`_.

.. _Jan Dittberner: https://jan.dittberner.info/
.. _Sphinx: http://www.sphinx-doc.org/
